const express = require('express');
const server = express();

server.use(express.json());
server.get('/', (req, res) => {
  res.json({message: 'OK'});
});

server.listen(3000, () => console.log('listening on localhost:3000'));