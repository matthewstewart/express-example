# Express Example
Going over some of the basics of an Express API.  
These examples use Visual Studio Code using a bash terminal.

## Walkthrough

### Setup Project Directory
Create working directory for the API:
```
mkdir express-example && cd express-example
```

### Initialize NPM With Defaults
```
npm init -y
```

### Install Express
```
npm install express
```
What does this command do?
```mermaid
graph TD
  A[npm install express] --> B[add/update express dependency in package.json]
  B --> C[Download express module from https://registry.npmjs.org]
	C --> D[Install in node_modules directory]
  D --> E[express now available using require]
```
### Create Server
Create a new file, server.js:
```
touch server.js
```
Add the following content to server.js:
```js
const express = require('express');
const server = express();

server.use(express.json());
server.get('/', (req, res) => {
  res.json({message: 'OK'});
});

server.listen(3000, () => console.log('listening on localhost:3000'));
```
#### What Does This Code Do?
```mermaid
graph TD
  A[Client] --> |GET localhost:3000| B[Express Server]
  B --> |Match Request To| C[Route GET '/']
  C --> |contains| D[Request]
  C --> |contains| E[Response]
  D --> |used for| F[Route Handler Function]
  F --> |which sets JSON| E
  E --> |send response to client| A
```

### Start Server
```
node server
```
You should see the message in your terminal:
```
listening on localhost:3000
```
Open up localhost:3000 as you would any website in your web browser and you will see:
```js
{"message":"OK"}
```

### Congratulations
You just wrote your first JSON API using Express.  
Sure it only has one endpoint, the root '/' route.  
This is a starting point, more to come.


